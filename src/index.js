const Sentry = require('@sentry/node')
var log4js = require('log4js')
var logger = log4js.getLogger()
logger.level = 'info'
const server = require('./server')
const { postgressConnect, prodSpecs } = require('./db/pg')
const { dbinit } = require('./db/init')
const { refreshRoutes } = require('./refreshRoutes')
const auth = require('./auth/authRoutes')
//const { refreshCustomRoutes } = require('./customRoutes')
const port = 3000

Sentry.init({
  dsn: 'https://d06473e562d74397a1433f63a52430e0@sentry.io/1439248'
})

dbinit().then(async () => {
  await refreshRoutes({
    server,
    db: postgressConnect(prodSpecs),
    message: 'initialization'
  })
  server.listen(port, () =>
    logger.info(`cblocks-api listening on port ${port}!`)
  )
})
