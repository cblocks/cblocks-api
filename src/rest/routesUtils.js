const Sentry = require('@sentry/node')
var log4js = require('log4js')
var logger = log4js.getLogger()
const { graphqlQuery } = require('./../graphql/graphql')

const formatFieldsAnswer = (fields, dataset) => {
  let data = dataset
  fields.forEach(field => {
    switch (field.fieldtype.id) {
      case 1:
        data[field.name] = data[field.name].id
        break
      case 2:
        data[field.name] = data[field.name].map(thingy => thingy.id)
        break
    }
  })
  return data
}


const addGraphqlQueryRouteToServer = (server, url, createQuery, formatData) => {
  server.get(url, (req, res, next) => {
    graphqlQuery(createQuery(req)).then(gQLresponse => {
      if (gQLresponse.errors) {
        logger.error(gQLresponse.errors)
        Sentry.captureException(gQLresponse.errors)
        next(gQLresponse.errors)
      } else {
        const answer = formatData(gQLresponse.data)
        res.setHeader('Content-Type', 'application/json')
        res.send(answer)
      }
    })
  })
}

module.exports.addGraphqlQueryRouteToServer = addGraphqlQueryRouteToServer
module.exports.formatFieldsAnswer = formatFieldsAnswer