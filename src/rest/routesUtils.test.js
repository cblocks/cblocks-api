const server = require('../server')
const { formatFieldsAnswer, addGraphqlQueryRouteToServer } = require('./routesUtils')

const startTestApi = async () => {
  await clearAllTables(pgTest)
  await writeDemoData(pgTest, '001')
  await initGraphql({ server, db: pgTest })
  await mountRest(server)
}


describe('Test Function "formatFieldsAnswer"', () => {
  test('formatFieldsAnswer with value field', async () => {
    const fields = [
      { id: 1, name: 'Title', fieldtype: { id: 3 } },
    ]
    const dataset = {
      id: 1,
      Title: 'Morgen geht die Welt unter!',
    }
    const response = formatFieldsAnswer(fields, dataset)
    expect(response).toEqual(
	    {id: 1, Title: "Morgen geht die Welt unter!"}
    )
  })

  test('formatFieldsAnswer with 1-connection field', async () => {
    const fields = [
      {id: 2, name: 'Company', fieldtype: { id: 1 } }
    ]
    const dataset = {
      id: 1,
      Company: { id: 4 }
    }
    const response = formatFieldsAnswer(fields, dataset)
    expect(response).toEqual(
	    {id: 1, Company: 4}
    )
  })

  test('formatFieldsAnswer with multi-connection field', async () => {
    const fields = [
      { id: 5, name: 'Requirements', fieldtype: { id: 2 } }
    ]
    const dataset = {
      id: 1,
      Requirements: [ { id: 5 }, { id: 7 } ]
    }
    const response = formatFieldsAnswer(fields, dataset)
    expect(response).toEqual(
	    {id: 1, Requirements: [5,7]}
    )
  })

})
