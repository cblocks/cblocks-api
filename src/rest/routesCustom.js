const Sentry = require('@sentry/node')
var log4js = require('log4js')
var logger = log4js.getLogger()
const { graphqlQuery } = require('./../graphql/graphql')
const { addGraphqlQueryRouteToServer } = require('./routesUtils')
const routePrefix = 'routes/'

const getAllRoutes = async () => {
  const promise = new Promise((resolve, reject) => {
    graphqlQuery('query{routeList{name query}}')
      .then(gQLresponse => {
        if (!gQLresponse.error) {
          resolve(gQLresponse.data.routeList)
        }
      })
      .catch(e => {
        resolve([])
      })
  })
  return promise
}

const mountCustomRoutes = async server => {
  const allRoutes = await getAllRoutes()
  allRoutes.forEach(route => {
    addGraphqlQueryRouteToServer(
      server,
      '/' + routePrefix + route.name,
      () => route.query,
      a => a
    )
  })
}

module.exports.mountCustomRoutes = mountCustomRoutes
