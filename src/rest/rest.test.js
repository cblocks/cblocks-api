const request = require('supertest')
const jwt = require('jsonwebtoken')
const server = require('../server')
const { postgressConnect, mainSpecs, testSpecs } = require('../db/pg')
const { writeData } = require('../db/dbUtils')
const { initGraphql } = require('../graphql/graphql')
const { dbInitForTests, dropDb } = require('../db/init')
const { mountRestRoutes } = require('./routesRest.js')

let pgTest
const authToken = jwt.sign({}, 'please-let-me-pass-psit4')
const startTestApi = async () => {}

beforeAll(async () => {
  await dbInitForTests()
  pgTest = postgressConnect(testSpecs)
  await writeData(pgTest, '003_')
  await initGraphql({ server, db: pgTest })
  await mountRestRoutes(server)
})

afterAll(async () => {
  pgTest.pool.end()
  const mainDb = postgressConnect(mainSpecs)
  await dropDb(mainDb, testSpecs)
  mainDb.pool.end()
})

describe('Test if the REST "/rest/job" delivers', () => {
  test('It should respond to the GET method with correct data', async () => {
    const response = await request(server)
      .get('/rest/job')
      .set('Authorization', 'bearer ' + authToken)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    expect(JSON.parse(response.text)).toEqual([
      {
        id: 1,
        Title: 'Programmier-Spezi',
        Company: 4,
        Posted: '2019-03-30',
        Deadline: '2019-05-30',
        Requirements: [5, 7]
      },
      {
        id: 2,
        Title: 'Web Bastler',
        Company: 3,
        Posted: '2019-03-30',
        Deadline: '2019-05-30',
        Requirements: [6]
      }
    ])
  })
})

describe('Test if the REST "/rest/job/1" delivers', () => {
  test('It should respond to the GET method', async () => {
    const response = await request(server)
      .get('/rest/job/1')
      .set('Authorization', 'bearer ' + authToken)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    expect(JSON.parse(response.text)).toEqual({
      id: 1,
      Title: 'Programmier-Spezi',
      Company: 4,
      Posted: '2019-03-30',
      Deadline: '2019-05-30',
      Requirements: [5, 7]
    })
  })
})

describe('Test if the REST "/rest/requirement" delivers', () => {
  test('It should respond to the GET method with correct data', async () => {
    const response = await request(server)
      .get('/rest/requirement')
      .set('Authorization', 'bearer ' + authToken)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    expect(JSON.parse(response.text)).toEqual([
      { id: 5, Requirement: 'C#' },
      { id: 6, Requirement: 'Javascript' },
      { id: 7, Requirement: 'Java' }
    ])
  })
})

describe('Test if the REST "/rest/requirement/6" delivers', () => {
  test('It should respond to the GET method', async () => {
    const response = await request(server)
      .get('/rest/requirement/6')
      .set('Authorization', 'bearer ' + authToken)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    expect(JSON.parse(response.text)).toEqual({
      id: 6,
      Requirement: 'Javascript'
    })
  })
})
