const request = require('supertest')
const jwt = require('jsonwebtoken')
const server = require('../server')
const { postgressConnect, mainSpecs, testSpecs } = require('../db/pg')
const { writeData } = require('../db/dbUtils')
const { initGraphql } = require('../graphql/graphql')
const { dbInitForTests, dropDb } = require('../db/init')
const { mountCustomRoutes } = require('./routesCustom.js')

let pgTest
const authToken = jwt.sign({}, 'please-let-me-pass-psit4')
const startTestApi = async () => {}

beforeAll(async () => {
  await dbInitForTests()
  pgTest = postgressConnect(testSpecs)
  await writeData(pgTest, '003_')
  await initGraphql({ server, db: pgTest })
  await mountCustomRoutes(server)
})

afterAll(async () => {
  pgTest.pool.end()
  const mainDb = postgressConnect(mainSpecs)
  await dropDb(mainDb, testSpecs)
  mainDb.pool.end()
})

describe('Test if the the custom route "/routes/allJobsComplete" delivers', () => {
  test('It should respond to the GET method with correct data', async () => {
    const response = await request(server)
      .get('/routes/allJobsComplete')
      .set('Authorization', 'bearer ' + authToken)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    expect(JSON.parse(response.text)).toEqual({
      JobList: [
        {
          id: 1,
          Title: 'Programmier-Spezi',
          Posted: '2019-03-30',
          Deadline: '2019-05-30',
          Company: {
            id: 4,
            Name: 'Microsoft'
          },
          Requirements: [
            { id: 5, Requirement: 'C#' },
            { id: 7, Requirement: 'Java' }
          ]
        },
        {
          id: 2,
          Title: 'Web Bastler',
          Posted: '2019-03-30',
          Deadline: '2019-05-30',
          Company: {
            id: 3,
            Name: 'Apple'
          },
          Requirements: [{ id: 6, Requirement: 'Javascript' }]
        }
      ]
    })
  })
})
