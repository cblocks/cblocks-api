const Sentry = require('@sentry/node')
var log4js = require('log4js')
var logger = log4js.getLogger()
const { graphqlQuery } = require('./../graphql/graphql')
const { addGraphqlQueryRouteToServer, formatFieldsAnswer } = require('./routesUtils')
const restprefix = 'rest/'

const getAllTypes = async () => {
  const promise = new Promise((resolve, reject) => {
    graphqlQuery(
      'query{typeList{id name description fields {id name fieldtype {id}}}}'
    )
      .then(gQLresponse => {
        if (!gQLresponse.error) {
          resolve(gQLresponse.data.typeList)
        }
      })
      .catch(e => {
        resolve([])
      })
  })
  return promise
}

const createQueryFields = fields => {
  const fieldQueryString = fields.reduce((x, y) => {
    if (y.fieldtype.id === 1 || y.fieldtype.id === 2) {
      return x + ' ' + y.name + '{id}'
    } else {
      return x + ' ' + y.name
    }
  }, ' id')
  return fieldQueryString
}

const mountRouteGetAll = (server, allTypes) => {
  allTypes.forEach(type => {
    const url = '/' + restprefix + type.name
    const createQuery = req =>
      'query{' + type.name + 'List{' + createQueryFields(type.fields) + '}}'
    addGraphqlQueryRouteToServer(server, url, createQuery, answer => {
      const datasets = answer[type.name + 'List']
      const datasetsformated = datasets.map(dataset =>
        formatFieldsAnswer(type.fields, dataset)
      )
      return datasetsformated
    })
  })
}

const mountRouteGetOne = (server, allTypes) => {
  allTypes.forEach(type => {
    const url = '/' + restprefix + type.name + '/:id'
    const createQuery = req =>
      'query{' +
      type.name +
      '(id:' +
      req.params.id +
      '){' +
      createQueryFields(type.fields) +
      '}}'
    addGraphqlQueryRouteToServer(server, url, createQuery, answer => {
      return formatFieldsAnswer(type.fields, answer[type.name])
    })
  })
}

const mountRestRoutes = async server => {
  const allTypes = await getAllTypes()
  mountRouteGetAll(server, allTypes)
  mountRouteGetOne(server, allTypes)
}

module.exports.mountRestRoutes = mountRestRoutes
