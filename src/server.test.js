const request = require('supertest')
const jwt = require('jsonwebtoken')
const server = require('./server')

const authToken = jwt.sign({}, 'please-let-me-pass-psit4')

describe('Test the root path', () => {
  test('It should require authentication', async () => {
    const response = await request(server).get('/')
    expect(response.statusCode).toBe(401)
  })
  test('It should pass authentication', async () => {
    const response = await request(server)
      .get('/')
      .set('Authorization', 'bearer ' + authToken)
    expect(response.statusCode).toBe(200)
  })
})
