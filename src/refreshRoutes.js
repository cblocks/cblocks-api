const { initGraphql } = require('./graphql/graphql')
const { mountRestRoutes } = require('./rest/routesRest')
const { mountCustomRoutes } = require('./rest/routesCustom')
var log4js = require('log4js')
var logger = log4js.getLogger()

const removeMiddlewares = (route, i, routes) => {
  if (
    route.handle.name === 'graphqlMiddleware' ||
    (route.path && route.path.match('^/rest/'))
  ) {
    routes.splice(i, 1)
  }
}

const deleteRoutes = server => {
  const routes = server._router.stack
  routes.forEach(removeMiddlewares)
}

const refreshRoutes = async ({ server, db, message }) => {
  deleteRoutes(server)
  const schema = await initGraphql({ server, db, refreshRoutes })
  logger.info('graphql mounted: ' + message)
  await mountRestRoutes(server)
  logger.info('rest routes mounted: ' + message)
  await mountCustomRoutes(server)
  logger.info('custom routes mounted: ' + message)
}

module.exports.refreshRoutes = refreshRoutes
module.exports.deleteRoutes = deleteRoutes
