const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const passportJWT = require('passport-jwt')
const JWTStrategy = passportJWT.Strategy
const ExtractJWT = passportJWT.ExtractJwt

const adminUser = { username: 'admin', password: 'psit4' }

passport.use(
  new LocalStrategy((username, password, done) => {
    if (username === adminUser.username && password === adminUser.password) {
      done(null, adminUser)
    } else {
      done(null, false)
    }
  })
)

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'please-let-me-pass-psit4'
    },
    (jwtPayload, cb) => {
      return cb(null, jwtPayload)
    }
  )
)

module.exports = passport
