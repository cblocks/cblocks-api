const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()
const passport = require('./auth/passport')
const authRoutes = require('./auth/authRoutes')

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(passport.initialize())
app.use('/auth', authRoutes)
app.all('*', passport.authenticate('jwt', { session: false }))
app.get('/', (req, res) => res.send('Hello World!'))

module.exports = app
