const joinMonster = require('join-monster').default
const {
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt
} = require('graphql')

const { defaultFieldTypes } = require('./constants')

const createTarget = (fieldId, type) => ({
  type,
  junction: {
    sqlTable: 'public.link',
    sqlJoins: [
      (postTable, linkTable) =>
        `${postTable}.id = ${linkTable}.post AND ${fieldId} = ${linkTable}.field`,
      (linkTable, targetTable) => `${linkTable}.target = ${targetTable}.id`
    ]
  }
})

const buildCreateTargets = (key1, key2) => (fieldId, type) => ({
  type: GraphQLList(type),
  junction: {
    sqlTable: 'public.link',
    sqlJoins: [
      (postTable, linkTable) =>
        `${postTable}.id = ${linkTable}.${key1} AND ${fieldId} = ${linkTable}.field`,
      (linkTable, targetTable) => `${linkTable}.${key2} = ${targetTable}.id`
    ]
  }
})

const createTargets = buildCreateTargets('post', 'target')

const createTargetedBy = buildCreateTargets('target', 'post')

const createValue = id => ({
  type: GraphQLString,
  sqlExpr: table =>
    `(SELECT v.value FROM public.value v WHERE v.post = ${table}.id AND v.field = ${id})`
})

const createAppropriateValue = (postTypes, { id, fieldtype, target }) => {
  switch (fieldtype) {
    case defaultFieldTypes.targetedby:
      return createTargetedBy(id, postTypes[target])
    case defaultFieldTypes.connection:
      return createTarget(id, postTypes[target])
    case defaultFieldTypes.connections:
      return createTargets(id, postTypes[target])
    default:
      return createValue(id)
  }
}

const createValues = (fields, postTypes) => {
  return fields.reduce(
    (acc, f) => ({ ...acc, [f.name]: createAppropriateValue(postTypes, f) }),
    {}
  )
}

const createPostType = ({ name, id, fields }, postTypes) => {
  const type = new GraphQLObjectType({
    description: 'a user defined content type',
    name,
    fields: () => {
      const fieldDefinitions = createValues(fields, postTypes)
      return {
        id: {
          type: GraphQLInt
        },
        ...fieldDefinitions
      }
    }
  })
  type._typeConfig = {
    sqlTable: `(SELECT id, type FROM public.post WHERE type = ${id})`,
    uniqueKey: 'id'
  }
  return type
}

const buildQueryAll = (db, graphqlObject) => ({
  type: GraphQLList(graphqlObject),
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

const buildQueryById = (db, graphqlObject) => ({
  type: graphqlObject,
  args: {
    id: {
      type: GraphQLNonNull(GraphQLInt)
    }
  },
  where: (postTable, args) => {
    return `${postTable}.id = ${args.id}`
  },
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

/**
 * build GraphQLObjectTypes for user defined types
 * @function
 * @param {array} typeDefinitions - an array of type definition objects
 * @return {object} an object containing [name querydefinition] entrys representing user defined types
 */
const buildPostTypes = typeDefinitions => {
  const postTypes = {}
  typeDefinitions.forEach(
    typeDefinition =>
      (postTypes[typeDefinition.id] = createPostType(typeDefinition, postTypes))
  )
  return postTypes
}

/**
 * build query entrypoints for user defined types
 * @function
 * @param {object} postTypes - an object of [name GraphQLObjectTypes] entrys
 * @return {object} an object containing [name querydefinition] entrys representing user defined types
 */
const buildQueryEntrypointsGenerated = (db, postTypes) => {
  return Object.values(postTypes).reduce(
    (acc, g) => ({
      ...acc,
      [`${g.name}List`]: buildQueryAll(db, g),
      [g.name]: buildQueryById(db, g)
    }),
    {}
  )
}

module.exports.buildPostTypes = buildPostTypes
module.exports.buildQueryEntrypointsGenerated = buildQueryEntrypointsGenerated
