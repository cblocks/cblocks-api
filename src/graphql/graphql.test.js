const request = require('supertest')
const jwt = require('jsonwebtoken')
const server = require('../server')
const { postgressConnect, mainSpecs, testSpecs } = require('../db/pg')
const { writeData } = require('../db/dbUtils')
const { initGraphql } = require('./graphql')
const { dbInitForTests, dropDb } = require('../db/init')

let pgTest
const authToken = jwt.sign({}, 'please-let-me-pass-psit4')
const startTestApi = async () => {}

beforeAll(async () => {
  await dbInitForTests()
  pgTest = postgressConnect(testSpecs)
  await writeData(pgTest, '001_')
  await initGraphql({ server, db: pgTest })
})

afterAll(async () => {
  pgTest.pool.end()
  const mainDb = postgressConnect(mainSpecs)
  await dropDb(mainDb, testSpecs)
  mainDb.pool.end()
})

describe('Test if the api is available', () => {
  test('It should respond to the GET method', async () => {
    const response = await request(server)
      .get('/graphql')
      .set('Authorization', 'bearer ' + authToken)
      .send(`query={typeList{id}}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    expect(JSON.parse(response.text)).toEqual({
      data: { typeList: [{ id: 1 }, { id: 2 }] }
    })
  })
  test('It should respond to the POST method', async () => {
    const response = await request(server)
      .post('/graphql')
      .set('Authorization', 'bearer ' + authToken)
      .send({ query: `{typeList{id}}` })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    expect(JSON.parse(response.text)).toEqual({
      data: { typeList: [{ id: 1 }, { id: 2 }] }
    })
  })
})
