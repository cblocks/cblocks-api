const create = require('../db/create')
const update = require('../db/update')
const del = require('../db/delete')
const {
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLList
} = require('graphql')

const buildField = f => {
  switch (f.fieldtype) {
    case 1:
      return {
        type: GraphQLInt
      }
    case 2:
      return {
        type: GraphQLNonNull(GraphQLList(GraphQLNonNull(GraphQLInt)))
      }
    default:
      return {
        type: GraphQLString
      }
  }
}

const buildRawType = td =>
  new GraphQLObjectType({
    name: `raw${td.name}`,
    fields: td.fields.reduce(
      (acc, f) => ({ ...acc, [f.name]: buildField(f) }),
      { id: { type: GraphQLNonNull(GraphQLInt) } }
    )
  })

const buildCreatePostInput = td =>
  new GraphQLInputObjectType({
    name: `create${td.name}Input`,
    fields: td.fields.reduce(
      (acc, f) => ({ ...acc, [f.name]: buildField(f) }),
      {}
    )
  })

const buildUpdatePostInput = td =>
  new GraphQLInputObjectType({
    name: `update${td.name}Input`,
    fields: td.fields.reduce(
      (acc, f) => ({ ...acc, [f.name]: buildField(f) }),
      { id: { type: GraphQLNonNull(GraphQLInt) } }
    )
  })

const buildDeletePostInput = td =>
  new GraphQLInputObjectType({
    name: `delete${td.name}Input`,
    fields: {
      id: {
        type: GraphQLNonNull(GraphQLInt)
      }
    }
  })

const buildCreate = (db, typeDefinition) => ({
  type: typeDefinition.rawType,
  args: {
    input: {
      type: buildCreatePostInput(typeDefinition)
    }
  },
  resolve: (parent, args) => create.generatedPost({ db, typeDefinition }, args)
})

const buildUpdate = (db, typeDefinition) => ({
  type: typeDefinition.rawType,
  args: {
    input: {
      type: buildUpdatePostInput(typeDefinition)
    }
  },
  resolve: (parent, args) => update.generatedPost({ db, typeDefinition }, args)
})

const buildDelete = (db, typeDefinition) => ({
  type: new GraphQLObjectType({
    name: `delete${typeDefinition.name}Output`,
    fields: {
      id: {
        type: GraphQLNonNull(GraphQLInt)
      }
    }
  }),
  args: {
    input: {
      type: buildDeletePostInput(typeDefinition)
    }
  },
  resolve: (parent, args) => del.generatedPost({ db, typeDefinition }, args)
})

const buildMutationEntrypointsGenerated = (db, typeDefinitions) => {
  typeDefinitions.forEach((td, i) => {
    typeDefinitions[i].rawType = buildRawType(td)
  })
  const MutationEntrypointsGenerated = typeDefinitions.reduce((acc, td) => {
    acc['create' + td.name] = buildCreate(db, td)
    acc['update' + td.name] = buildUpdate(db, td)
    acc['delete' + td.name] = buildDelete(db, td)
    return acc
  }, {})
  return MutationEntrypointsGenerated
}

module.exports.buildMutationEntrypointsGenerated = buildMutationEntrypointsGenerated
