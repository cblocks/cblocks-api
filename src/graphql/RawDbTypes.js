const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLString,
  GraphQLNonNull,
  GraphQLBoolean
} = require('graphql')

const Route = new GraphQLObjectType({
  description: 'a route to query the api',
  name: 'rawRoute',
  fields: () => ({
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    query: {
      type: GraphQLNonNull(GraphQLString)
    }
  })
})

const Field = new GraphQLObjectType({
  description: 'describes a field of a content type',
  name: 'rawField',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    index: {
      type: GraphQLNonNull(GraphQLInt)
    },
    fieldtype: {
      type: GraphQLNonNull(GraphQLInt)
    },
    type: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    target: {
      type: GraphQLInt
    },
    description: {
      type: GraphQLString
    },
    shortform: {
      type: GraphQLNonNull(GraphQLBoolean)
    }
  })
})

const Fieldtype = new GraphQLObjectType({
  description: 'describes the kind of data stored in a field',
  name: 'rawFieldtype',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    }
  })
})

const Type = new GraphQLObjectType({
  description: 'a content type',
  name: 'rawType',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    }
  })
})

module.exports.Field = Field
module.exports.Fieldtype = Fieldtype
module.exports.Type = Type
module.exports.Route = Route
