const del = require('../db/delete')
const { Route, Type, Fieldtype, Field } = require('./RawDbTypes')
const {
  deleteRouteInput,
  deleteTypeInput,
  deleteFieldtypeInput,
  deleteFieldInput
} = require('./DeleteInputTypes')

const deleteRoute = ctx => ({
  type: Route,
  args: {
    input: {
      type: deleteRouteInput
    }
  },
  resolve: (parent, args) => del.route(ctx, args)
})

const deleteType = ctx => ({
  type: Type,
  args: {
    input: {
      type: deleteTypeInput
    }
  },
  resolve: (parent, args) => del.type(ctx, args)
})

const deleteFieldtype = ctx => ({
  type: Fieldtype,
  args: {
    input: {
      type: deleteFieldtypeInput
    }
  },
  resolve: (parent, args) => del.fieldtype(ctx, args)
})

const deleteField = ctx => ({
  type: Field,
  args: {
    input: {
      type: deleteFieldInput
    }
  },
  resolve: (parent, args) => del.field(ctx, args)
})

module.exports = ctx => ({
  deleteType: deleteType(ctx),
  deleteFieldtype: deleteFieldtype(ctx),
  deleteField: deleteField(ctx),
  deleteRoute: deleteRoute(ctx)
})
