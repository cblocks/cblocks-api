const {
  GraphQLList,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString
} = require('graphql')
const joinMonster = require('join-monster').default
const { Route, Field, Fieldtype, Type } = require('./DbTypes')

const routeList = db => ({
  type: GraphQLList(Route),
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

const route = db => ({
  type: Route,
  args: {
    name: {
      type: GraphQLNonNull(GraphQLString)
    }
  },
  where: (routeTable, args) => {
    return `${routeTable}.name = ${args.id}`
  },
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

const typeList = db => ({
  type: GraphQLList(Type),
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

const type = db => ({
  type: Type,
  args: {
    id: {
      description: 'The Type ID number',
      type: GraphQLNonNull(GraphQLInt)
    }
  },
  where: (typeTable, args) => {
    return `${typeTable}.id = ${args.id}`
  },
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

const fieldList = db => ({
  type: GraphQLList(Field),
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

const field = db => ({
  type: Field,
  args: {
    id: {
      description: 'The Field ID number',
      type: GraphQLNonNull(GraphQLInt)
    }
  },
  where: (fieldTable, args) => {
    return `${fieldTable}.id = ${args.id}`
  },
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

const fieldtypeList = db => ({
  type: GraphQLList(Fieldtype),
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

const fieldtype = db => ({
  type: Fieldtype,
  args: {
    id: {
      description: 'The Fieldtype ID number',
      type: GraphQLNonNull(GraphQLInt)
    }
  },
  where: (fieldtypeTable, args) => {
    return `${fieldtypeTable}.id = ${args.id}`
  },
  resolve: (parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, context, sql => db.query(sql))
  }
})

/**
 * an object containing [name GraphQLObjectType] entrys representing db tables
 * @object
 */
const QueryEntryPointsDb = db => ({
  route: route(db),
  routeList: routeList(db),
  typeList: typeList(db),
  type: type(db),
  fieldtypeList: fieldtypeList(db),
  fieldtype: fieldtype(db),
  fieldList: fieldList(db),
  field: field(db)
})

module.exports.QueryEntryPointsDb = QueryEntryPointsDb
