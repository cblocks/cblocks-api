const {
  GraphQLNonNull,
  GraphQLInt,
  GraphQLInputObjectType,
  GraphQLString
} = require('graphql')

const deleteRouteInput = new GraphQLInputObjectType({
  name: 'deleteRouteInput',
  fields: () => ({
    name: {
      type: GraphQLNonNull(GraphQLString)
    }
  })
})

const deleteTypeInput = new GraphQLInputObjectType({
  name: 'deleteTypeInput',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    }
  })
})

const deleteFieldtypeInput = new GraphQLInputObjectType({
  name: 'deleteFieldtypeInput',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    }
  })
})

const deleteFieldInput = new GraphQLInputObjectType({
  name: 'deleteFieldInput',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    }
  })
})

module.exports.deleteTypeInput = deleteTypeInput
module.exports.deleteFieldtypeInput = deleteFieldtypeInput
module.exports.deleteFieldInput = deleteFieldInput
module.exports.deleteRouteInput = deleteRouteInput
