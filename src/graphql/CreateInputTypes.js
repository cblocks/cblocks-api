const {
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
  GraphQLInputObjectType,
  GraphQLBoolean
} = require('graphql')

const createRouteInput = new GraphQLInputObjectType({
  name: 'createRouteInput',
  fields: () => ({
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    query: {
      type: GraphQLNonNull(GraphQLString)
    }
  })
})

const createTypeInput = new GraphQLInputObjectType({
  name: 'createTypeInput',
  fields: () => ({
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    }
  })
})

const createFieldtypeInput = new GraphQLInputObjectType({
  name: 'createFieldtypeInput',
  fields: () => ({
    index: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    }
  })
})

const createFieldInput = new GraphQLInputObjectType({
  name: 'createFieldInput',
  fields: () => ({
    type: {
      type: GraphQLNonNull(GraphQLInt)
    },
    fieldtype: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    target: {
      type: GraphQLInt
    },
    description: {
      type: GraphQLString
    },
    index: {
      type: GraphQLInt
    },
    shortform: {
      type: GraphQLNonNull(GraphQLBoolean)
    }
  })
})

module.exports.createTypeInput = createTypeInput
module.exports.createFieldtypeInput = createFieldtypeInput
module.exports.createFieldInput = createFieldInput
module.exports.createRouteInput = createRouteInput
