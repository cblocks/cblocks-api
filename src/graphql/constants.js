const defaultFieldTypes = {
  targetedby: 0,
  connection: 1,
  connections: 2
}

const nameRegex = /^[a-zA-Z0-9]*$/
const createRegex = /^create/
const updateRegex = /^update/
const deleteRegex = /^delete/
const rawRegex = /^raw/
const typeRegex = /^type$/
const fieldtypeRegex = /^fieldtype$/
const fieldRegex = /^field$/
const routeRegex = /^route$/
const listRegex = /List$/
const targetedByRegex = /^referencedBy/

const checkReservedName = name => {
  if (name.match(nameRegex) == null) {
    throw new Error('names must be alphanumeric')
  } else if (name.match(typeRegex) != null) {
    throw new Error('"type" is a reserved word')
  } else if (name.match(fieldtypeRegex) != null) {
    throw new Error('"fieldtype" is a reserved word')
  } else if (name.match(fieldRegex) != null) {
    throw new Error('"field" is a reserved word')
  } else if (name.match(routeRegex) != null) {
    throw new Error('"route" is a reserved word')
  } else if (name.match(createRegex) != null) {
    throw new Error('names must not start with "create"')
  } else if (name.match(updateRegex) != null) {
    throw new Error('names must not start with "update"')
  } else if (name.match(deleteRegex) != null) {
    throw new Error('names must not start with "delete"')
  } else if (name.match(rawRegex) != null) {
    throw new Error('names must not start with "raw"')
  } else if (name.match(listRegex) != null) {
    throw new Error('names must not end with "List"')
  } else if (name.match(targetedByRegex) != null) {
    throw new Error('names must not start with "referencedBy"')
  }
}

module.exports.defaultFieldTypes = defaultFieldTypes
module.exports.checkReservedName = checkReservedName
