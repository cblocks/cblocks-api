const update = require('../db/update')
const { Route, Type, Fieldtype, Field } = require('./RawDbTypes')
const {
  updateRouteInput,
  updateTypeInput,
  updateFieldtypeInput,
  updateFieldInput
} = require('./UpdateInputTypes')

const updateRoute = ctx => ({
  type: Route,
  args: {
    input: {
      type: updateRouteInput
    }
  },
  resolve: (parent, args) => update.route(ctx, args)
})

const updateType = ctx => ({
  type: Type,
  args: {
    input: {
      type: updateTypeInput
    }
  },
  resolve: (parent, args) => update.type(ctx, args)
})

const updateFieldtype = ctx => ({
  type: Fieldtype,
  args: {
    input: {
      type: updateFieldtypeInput
    }
  },
  resolve: (parent, args) => update.fieldtype(ctx, args)
})

const updateField = ctx => ({
  type: Field,
  args: {
    input: {
      type: updateFieldInput
    }
  },
  resolve: (parent, args) => update.field(ctx, args)
})

module.exports = ctx => ({
  updateType: updateType(ctx),
  updateFieldtype: updateFieldtype(ctx),
  updateField: updateField(ctx),
  updateRoute: updateRoute(ctx)
})
