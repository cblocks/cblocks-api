const Sentry = require('@sentry/node')
var log4js = require('log4js')
var logger = log4js.getLogger()
const { graphql, GraphQLSchema, GraphQLObjectType } = require('graphql')
const graphqlHTTP = require('express-graphql')
const { QueryEntryPointsDb } = require('./QueryEntrypointsDb')
const CreateEntryPointsDb = require('./CreateEntrypointsDb')
const UpdateEntryPointsDb = require('./UpdateEntrypointsDb')
const DeleteEntryPointsDb = require('./DeleteEntrypointsDb')

const {
  buildPostTypes,
  buildQueryEntrypointsGenerated
} = require('./QueryEntrypointsGenerated')
const {
  buildMutationEntrypointsGenerated
} = require('./MutationEntrypointsGenerated')

let schema

const mountSchema = (app, schema) => {
  app.use(
    '/graphql',
    graphqlHTTP({
      schema: schema,
      rootValue: global,
      graphiql: true
    })
  )
}

const parseTypeDefinitions = rows => {
  const rawTypeDefinitions = rows.reduce(
    (acc, row) =>
      acc[row.id]
        ? acc
        : {
            ...acc,
            [row.id]: {
              id: row.id,
              name: row.name,
              fields: rows.reduce(
                (a, r) =>
                  r.id != row.id
                    ? a
                    : [
                        ...a,
                        {
                          id: r.fid,
                          name: r.fname,
                          fieldtype: r.fieldtype,
                          target: r.target
                        }
                      ],
                []
              )
            }
          },
    {}
  )
  rows.forEach(r => {
    if (r.target) {
      rawTypeDefinitions[r.target].fields.push({
        id: r.fid,
        name: `referencedBy${r.fname}Of${r.name}`,
        fieldtype: 0,
        target: r.id
      })
    }
  })
  return Object.values(rawTypeDefinitions)
}

const buildSchema = (QueryEntryPoints, MutationEntryPoints) => {
  const query = {
    description: 'queries',
    name: 'Query',
    fields: () => QueryEntryPoints
  }

  const mutation = {
    description: 'mutations',
    name: 'Mutation',
    fields: () => MutationEntryPoints
  }

  const QueryRoot = new GraphQLObjectType(query)

  const MutationRoot = new GraphQLObjectType(mutation)

  const schema = new GraphQLSchema({
    description: 'cblocks api',
    query: QueryRoot,
    mutation: MutationRoot
  })

  return schema
}

const getEntrypointsGenerated = async db => {
  const sql = `SELECT t.id, t.name, f.id AS "fid", f.name AS "fname", f.fieldtype, c.type AS "target"
  FROM public.type t 
    JOIN public.field f ON t.id = f.type
    LEFT JOIN public.connection c ON c.field = f.id`
  const res = await db.query(sql)
  const typeDefinitions = parseTypeDefinitions(res.rows)
  const postTypes = buildPostTypes(typeDefinitions)
  typeDefinitions.forEach(td => (td.postType = postTypes[td.id]))
  const QueryEntrypointsGenerated = buildQueryEntrypointsGenerated(
    db,
    postTypes
  )
  const MutationEntrypointsGenerated = buildMutationEntrypointsGenerated(
    db,
    typeDefinitions
  )
  return { QueryEntrypointsGenerated, MutationEntrypointsGenerated }
}

const graphqlQuery = async (query, variables) => {
  return await graphql(schema, query, global, undefined, variables)
}

const getUserdefinedSchema = async db => {
  const EntrypointsGenerated = getEntrypointsGenerated(db)
  return buildSchema(EntrypointsGenerated)
}

const getDbSchema = db => {
  return buildSchema(QueryEntryPointsDb(db))
}

const getCombinedSchema = async ctx => {
  const {
    QueryEntrypointsGenerated,
    MutationEntrypointsGenerated
  } = await getEntrypointsGenerated(ctx.db)
  return buildSchema(
    { ...QueryEntryPointsDb(ctx.db), ...QueryEntrypointsGenerated },
    {
      ...CreateEntryPointsDb(ctx),
      ...UpdateEntryPointsDb(ctx),
      ...DeleteEntryPointsDb(ctx),
      ...MutationEntrypointsGenerated
    }
  )
}

const initGraphql = async ({ server, db, refreshRoutes }) => {
  schema = await getCombinedSchema({
    db,
    refreshSchema: message => refreshRoutes({ server, db, message })
  })
  await mountSchema(server, schema)
  return schema
}

module.exports.graphqlQuery = graphqlQuery
module.exports.initGraphql = initGraphql
module.exports.getDbSchema = getDbSchema
module.exports.getUserdefinedSchema = getUserdefinedSchema
module.exports.getCombinedSchema = getCombinedSchema
