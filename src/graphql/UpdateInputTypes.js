const {
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
  GraphQLInputObjectType,
  GraphQLBoolean
} = require('graphql')

const updateRouteInput = new GraphQLInputObjectType({
  name: 'updateRouteInput',
  fields: () => ({
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    query: {
      type: GraphQLNonNull(GraphQLString)
    }
  })
})

const updateTypeInput = new GraphQLInputObjectType({
  name: 'updateTypeInput',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    }
  })
})

const updateFieldtypeInput = new GraphQLInputObjectType({
  name: 'updateFieldtypeInput',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    }
  })
})

const updateFieldInput = new GraphQLInputObjectType({
  name: 'updateFieldInput',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    index: {
      type: GraphQLNonNull(GraphQLInt)
    },
    fieldtype: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    },
    shortform: {
      type: GraphQLNonNull(GraphQLBoolean)
    }
  })
})

module.exports.updateTypeInput = updateTypeInput
module.exports.updateFieldtypeInput = updateFieldtypeInput
module.exports.updateFieldInput = updateFieldInput
module.exports.updateRouteInput = updateRouteInput
