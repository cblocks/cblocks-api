const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull,
  GraphQLBoolean
} = require('graphql')

const Route = new GraphQLObjectType({
  description: 'a route',
  name: 'route',
  fields: () => ({
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    query: {
      type: GraphQLNonNull(GraphQLString)
    }
  })
})
Route._typeConfig = {
  sqlTable: 'public.route',
  uniqueKey: 'name'
}

const Field = new GraphQLObjectType({
  description: 'describes a field of a content type',
  name: 'field',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    index: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    },
    shortform: {
      type: GraphQLNonNull(GraphQLBoolean)
    },
    fieldtype: {
      type: GraphQLNonNull(Fieldtype),
      sqlJoin: (fieldTable, fieldtypeTable) =>
        `${fieldTable}.fieldtype = ${fieldtypeTable}.id`
    },
    type: {
      type: GraphQLNonNull(Type),
      sqlJoin: (fieldTable, typeTable) => `${fieldTable}.type = ${typeTable}.id`
    },
    target: {
      type: Type,
      junction: {
        sqlTable: 'public.connection',
        sqlJoins: [
          (fieldTable, connectionTable) =>
            `${fieldTable}.id = ${connectionTable}.field`,
          (connectionTable, typeTable) =>
            `${connectionTable}.type = ${typeTable}.id`
        ]
      }
    }
  })
})
Field._typeConfig = {
  sqlTable: 'public.field',
  uniqueKey: 'id'
}

const Fieldtype = new GraphQLObjectType({
  description: 'describes the kind of data stored in a field',
  name: 'fieldtype',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    },
    fields: {
      type: GraphQLNonNull(GraphQLList(GraphQLNonNull(Field))),
      sqlJoin: (fieldtypeTable, fieldTable) =>
        `${fieldtypeTable}.id = ${fieldTable}.fieldtype`
    }
  })
})
Fieldtype._typeConfig = {
  sqlTable: 'public.fieldtype',
  uniqueKey: 'id'
}

const Type = new GraphQLObjectType({
  description: 'a content type',
  name: 'type',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    label: {
      type: GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    },
    fields: {
      type: GraphQLNonNull(GraphQLList(GraphQLNonNull(Field))),
      sqlJoin: (typeTable, fieldTable) =>
        `${typeTable}.id = ${fieldTable}.type`,
      orderBy: {
        index: 'asc'
      }
    },
    count: {
      type: GraphQLNonNull(GraphQLInt),
      sqlExpr: table =>
        `(SELECT COUNT(id) FROM public.post p WHERE p.type = ${table}.id)`
    }
  })
})
Type._typeConfig = {
  sqlTable: 'public.type',
  uniqueKey: 'id'
}

module.exports.Field = Field
module.exports.Fieldtype = Fieldtype
module.exports.Type = Type
module.exports.Route = Route
