const create = require('../db/create')
const { Route, Type, Fieldtype, Field } = require('./RawDbTypes')
const {
  createRouteInput,
  createTypeInput,
  createFieldtypeInput,
  createFieldInput
} = require('./CreateInputTypes')

const createRoute = ctx => ({
  type: Route,
  args: {
    input: {
      type: createRouteInput
    }
  },
  resolve: (parent, args) => create.route(ctx, args)
})

const createType = ctx => ({
  type: Type,
  args: {
    input: {
      type: createTypeInput
    }
  },
  resolve: (parent, args) => create.type(ctx, args)
})

const createFieldtype = ctx => ({
  type: Fieldtype,
  args: {
    input: {
      type: createFieldtypeInput
    }
  },
  resolve: (parent, args) => create.fieldtype(ctx, args)
})

const createField = ctx => ({
  type: Field,
  args: {
    input: {
      type: createFieldInput
    }
  },
  resolve: (parent, args) => create.field(ctx, args)
})

module.exports = ctx => ({
  createType: createType(ctx),
  createFieldtype: createFieldtype(ctx),
  createField: createField(ctx),
  createRoute: createRoute(ctx)
})
