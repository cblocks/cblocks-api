const Sentry = require('@sentry/node')
var log4js = require('log4js')
var logger = log4js.getLogger()
const { Pool, Client } = require('pg')

const prodSpecs = {
  user: 'postgres',
  host: 'postgres',
  database: 'headless',
  password: 'root',
  port: 5432
}

const testSpecs = {
  user: 'postgres',
  host: 'postgres',
  database: 'test',
  password: 'root',
  port: 5432
}

const mainSpecs = {
  user: 'postgres',
  host: 'postgres',
  database: 'postgres',
  password: 'root',
  port: 5432
}

const postgressConnect = specs => {
  const pool = new Pool(specs)
  return {
    pool,
    query: async (query, args) => {
      const client = await pool.connect()
      try {
        return await client.query(query, args)
      } finally {
        client.release()
      }
    },
    client: () => new Client(specs)
  }
}

module.exports.postgressConnect = postgressConnect
module.exports.prodSpecs = prodSpecs
module.exports.testSpecs = testSpecs
module.exports.mainSpecs = mainSpecs
