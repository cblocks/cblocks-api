const Sentry = require('@sentry/node')
var log4js = require('log4js')
var logger = log4js.getLogger()
const { defaultFieldTypes, checkReservedName } = require('../graphql/constants')

const buildCreateFunction = (
  queryString,
  argumentExtractor,
  refreshMessage,
  errorMessage
) => ({ db, refreshSchema }, { input }) => {
  return db
    .query(queryString, argumentExtractor(input))
    .then(res => {
      refreshSchema(refreshMessage + input.name)
      return res.rowCount == 1 ? res.rows[0] : new Error(errorMessage)
    })
    .catch(e => {
      Sentry.captureException(e)
      logger.error(e)
      throw new Error(errorMessage)
    })
}

const route = buildCreateFunction(
  'INSERT INTO public.route (name, query) VALUES ($1, $2) RETURNING name, query',
  ({ name, query }) => [name, query],
  'reloaded api after creating route ',
  'unable to create route'
)

const type = buildCreateFunction(
  'INSERT INTO public.type (name, label, description) VALUES ($1, $2, $3) RETURNING id, name, label, description',
  ({ name, label, description }) => [name, label, description],
  'reloaded api after creating type ',
  'unable to create type'
)

const fieldtype = buildCreateFunction(
  'INSERT INTO public.fieldtype (name, label, description) VALUES ($1, $2, $3) RETURNING id, name, label, description',
  ({ name, label, description }) => [name, label, description],
  'reloaded api after creating fieldtype ',
  'unable to create fieldtype'
)

const field = (
  { db, refreshSchema },
  {
    input: {
      type,
      fieldtype,
      index,
      name,
      label,
      description,
      shortform,
      target
    }
  }
) => {
  checkReservedName(name)
  if (fieldtype == 1 || fieldtype == 2) {
    if (typeof target == 'number') {
      return db
        .query(
          'WITH entry AS (INSERT INTO public.field (type, index, fieldtype, name, label, description, shortform) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id, type, index, fieldtype, name, label, description, shortform), ' +
            'target AS (INSERT INTO public.connection (field, type) VALUES ((SELECT id from entry), $8) RETURNING type AS target) ' +
            'SELECT id, type, fieldtype, name, label, description, index, shortform, target FROM entry, target',
          [type, index, fieldtype, name, label, description, shortform, target]
        )
        .then(res => {
          refreshSchema(`reloaded api after creating field ${name}`)
          return res.rowCount == 1
            ? res.rows[0]
            : new Error('unable to create field')
        })
        .catch(e => {
          Sentry.captureException(e)
          logger.error(e)
          throw new Error(`unable to create field: ${e.detail}`)
        })
    } else {
      throw new Error('fields with fieldtype 1 or 2 must have a target')
    }
  } else {
    if (typeof target == Number) {
      throw new Error('only fields with fieldtype 1 or 2 can have a target')
    } else {
      return db
        .query(
          'INSERT INTO public.field (type, index, fieldtype, name, label, description, shortform) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id, type, index, fieldtype, name, label, description, shortform',
          [type, index, fieldtype, name, label, description, shortform]
        )
        .then(res => {
          refreshSchema(`reloaded api after creating field ${name}`)
          return res.rowCount == 1
            ? res.rows[0]
            : new Error('unable to create field')
        })
        .catch(e => {
          Sentry.captureException(e)
          logger.error(e)
          throw new Error(`unable to create field: ${e.detail}`)
        })
    }
  }
}

const dollarList = i => {
  const params = [...Array(i).keys()].map(i => `$${i + 1}`)
  return `(${params.join(',')})`
}

const insertString = (f, args, i) => {
  switch (f.fieldtype) {
    case defaultFieldTypes.connection:
      return linkString(f, i)
    case defaultFieldTypes.connections:
      return linksString(f, args[f.name], i)
    default:
      return valueString(f, i)
  }
}

const valueString = ({ name, id }, i) =>
  `, "${name}" AS (INSERT INTO public.value (post, field, value) VALUES ((SELECT id FROM entry),${id},$${i}) RETURNING value AS "${name}")`

const linkString = ({ name, id }, i) =>
  `, "${name}" AS (INSERT INTO public.link (post, field, target) VALUES ((SELECT id FROM entry),${id},$${i}) RETURNING target AS "${name}")`

const linksString = ({ name, id }, args, i) =>
  args.map(
    (nr, j) =>
      `, "${name +
        j}" AS (INSERT INTO public.link (post, field, target) VALUES ((SELECT id FROM entry),${id},$${i +
        j}) RETURNING target AS "${name + j}")`
  )

const updateParameterCount = (f, args, i) => {
  switch (f.fieldtype) {
    case defaultFieldTypes.connections:
      return i + args[f.name].length
    default:
      return i + 1
  }
}

const buildSqlForGeneratedPost = (typeDefinition, { input: args }) => {
  const [inserts, params] = typeDefinition.fields.reduce(
    ([inserts, params, i], f) =>
      args[f.name] == null || args[f.name] == undefined
        ? [inserts, params, i]
        : [
            f.fieldtype == 2
              ? [...inserts, ...insertString(f, args, i)]
              : [...inserts, insertString(f, args, i)],
            [...params, ...(f.fieldtype == 2 ? args[f.name] : [args[f.name]])],
            updateParameterCount(f, args, i)
          ],
    [[], [], 1]
  )

  const fromTables = typeDefinition.fields.reduce(
    (acc, f) =>
      args[f.name] == null || args[f.name] == undefined
        ? acc
        : f.fieldtype == 2
        ? [...acc, ...args[f.name].map((nr, i) => `"${f.name + i}"`)]
        : [...acc, `"${f.name}"`],
    []
  )

  const sql = `WITH entry AS ( INSERT INTO public.post (type) VALUES (${
    typeDefinition.id
  }) RETURNING id ) ${inserts.join(' ')} SELECT id ${
    fromTables.length == 0 ? '' : ','
  } ${fromTables.join(', ')} FROM entry ${
    fromTables.length == 0 ? '' : ','
  } ${fromTables.join(', ')};`
  return [sql, params]
}

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

const checkFkIntegrity = async (db, f, { input }) => {
  if (
    input[f.name] == null ||
    input[f.name] == undefined ||
    input[f.name].length == 0
  ) {
    return
  }
  let res = {}
  try {
    res =
      f.fieldtype == 1
        ? await db.query(`SELECT type FROM public.post WHERE id = $1`, [
            input[f.name]
          ])
        : await db.query(
            `SELECT type FROM public.post WHERE id IN ${dollarList(
              input[f.name].length
            )}`,
            input[f.name]
          )
  } catch (e) {
    Sentry.captureException(e)
    logger.error(e)
    throw new Error(`could not check referenced posts`)
  }
  if (
    f.fieldtype == 1
      ? res.rows.length != 1
      : res.rows.length != input[f.name].length
  ) {
    throw new Error(`not all referenced posts exist`)
  }
  if (res.rows.some(r => r.type != f.target)) {
    throw new Error(`not all referenced posts have the correct type`)
  }
}

const generatedPost = async ({ db, typeDefinition }, args) => {
  const typeFields = typeDefinition.fields.filter(
    f => f.fieldtype == 1 || f.fieldtype == 2
  )
  await asyncForEach(typeFields, f => checkFkIntegrity(db, f, args))
  return db
    .query(...buildSqlForGeneratedPost(typeDefinition, args))
    .then(res => {
      if (res.rowCount != 1) {
        throw Error('unable to create post')
      }
      const multikeys = typeDefinition.fields
        .filter(f => f.fieldtype == 2)
        .reduce((acc, f) => ({ ...acc, [`^${f.name}[0-9]*`]: f.name }), {})
      const accumulator = Object.entries(multikeys).reduce(
        (acc, [r, n]) => ({ ...acc, [n]: [] }),
        {}
      )
      return Object.entries(res.rows[0]).reduce((acc, [k, v]) => {
        const isMultikey = Object.entries(multikeys).find(([regex, name]) =>
          k.match(regex)
        )
        return isMultikey
          ? { ...acc, [isMultikey[1]]: [...acc[isMultikey[1]], v] }
          : { ...acc, [k]: v }
      }, accumulator)
    })
    .catch(e => {
      Sentry.captureException(e)
      logger.error(e)
      throw new Error(`unable to create post: ${e.detail}`)
    })
}

module.exports.type = type
module.exports.fieldtype = fieldtype
module.exports.field = field
module.exports.generatedPost = generatedPost
module.exports.route = route
