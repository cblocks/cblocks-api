const log4js = require('log4js')
const logger = log4js.getLogger()
const fs = require('fs')
const copyFrom = require('pg-copy-streams').from

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

const clearAllTables = db =>
  db
    .query(
      `TRUNCATE TABLE public.link CASCADE;
    TRUNCATE TABLE public.value CASCADE;
    TRUNCATE TABLE public.connection CASCADE;
    TRUNCATE TABLE public.field CASCADE;
    TRUNCATE TABLE public.fieldtype CASCADE;
    TRUNCATE TABLE public.type CASCADE;
    TRUNCATE TABLE public.route CASCADE;`
    )
    .catch(e => logger.error(e))

const writeDataForTable = async (db, tablename, prefix) => {
  const client = await db.pool.connect()
  try {
    await new Promise((resolve, reject) => {
      logger.debug(`writing data to ${tablename}`)
      var stream = client.query(
        copyFrom(
          `COPY public.${tablename} FROM STDIN DELIMITERS ',' CSV HEADER QUOTE '"' ESCAPE '\\'`
        )
      )
      var fileStream = fs.createReadStream(
        `src/db/data/${prefix ? prefix : ''}${tablename}.csv`
      )
      fileStream.on('error', reject)
      stream.on('error', reject)
      stream.on('end', resolve)
      fileStream.pipe(stream)
    })
    await db.query(
      `SELECT setval('public.${tablename}_id_seq', max(id)) FROM public.${tablename}`
    )
    logger.info(`finished writing to ${tablename}`)
  } catch (e) {
    logger.error(e)
  } finally {
    client.release()
  }
}

const writeData = async (db, prefix) => {
  await asyncForEach(
    [
      'type',
      'fieldtype',
      'field',
      'connection',
      'post',
      'value',
      'link',
      'route'
    ],
    async tablename => await writeDataForTable(db, tablename, prefix)
  )
}

module.exports.writeData = writeData
module.exports.clearAllTables = clearAllTables
