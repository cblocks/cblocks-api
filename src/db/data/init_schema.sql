BEGIN;
/* change this comment to force db-reinitialization 1.2*/

CREATE SEQUENCE public.route_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.connection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.fieldtype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.settings(
	key character varying,
	value character varying
);

CREATE TABLE public.route(
    id integer NOT NULL DEFAULT nextval('route_id_seq'::regclass),
	name character varying COLLATE pg_catalog.default NOT NULL,
	query character varying COLLATE pg_catalog.default NOT NULL,
    CONSTRAINT route_pkey PRIMARY KEY (name)
);

CREATE TABLE public.type
(
    id integer NOT NULL DEFAULT nextval('type_id_seq'::regclass),
    name character varying COLLATE pg_catalog.default NOT NULL,
    label character varying COLLATE pg_catalog.default NOT NULL,
    description character varying COLLATE pg_catalog.default,
    CONSTRAINT type_pkey PRIMARY KEY (id),
    CONSTRAINT type_name_key UNIQUE (name)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.type
    OWNER to postgres;

CREATE TABLE public.fieldtype
(
    id integer NOT NULL DEFAULT nextval('fieldtype_id_seq'::regclass),
    name character varying COLLATE pg_catalog.default NOT NULL,
    label character varying COLLATE pg_catalog.default NOT NULL,
    description character varying COLLATE pg_catalog.default,
    CONSTRAINT fieldtype_pkey PRIMARY KEY (id),
    CONSTRAINT fieldtype_name_key UNIQUE (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.fieldtype
    OWNER to postgres;

CREATE TABLE public.field
(
    id integer NOT NULL DEFAULT nextval('field_id_seq'::regclass),
    type integer NOT NULL,
    index integer NOT NULL,
    fieldtype integer NOT NULL,
    name character varying COLLATE pg_catalog.default NOT NULL,
    label character varying COLLATE pg_catalog.default NOT NULL,
    description character varying COLLATE pg_catalog.default,
    shortform boolean NOT NULL,
    CONSTRAINT field_pkey PRIMARY KEY (id),
    CONSTRAINT field_type_name_key UNIQUE (type, name)
,
    CONSTRAINT field_fieldtype_fkey FOREIGN KEY (fieldtype)
        REFERENCES public.fieldtype (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT field_type_fkey FOREIGN KEY (type)
        REFERENCES public.type (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.field
    OWNER to postgres;

CREATE TABLE public.connection
(
    id integer NOT NULL DEFAULT nextval('connection_id_seq'::regclass),
    field integer NOT NULL,
    type integer NOT NULL,
    CONSTRAINT connection_pkey PRIMARY KEY (id),
    CONSTRAINT connection_field_key UNIQUE (field)
,
    CONSTRAINT connection_field_fkey FOREIGN KEY (field)
        REFERENCES public.field (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT connection_type_fkey FOREIGN KEY (type)
        REFERENCES public.type (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.connection
    OWNER to postgres;

CREATE TABLE public.post
(
    id integer NOT NULL DEFAULT nextval('post_id_seq'::regclass),
    type integer NOT NULL,
    CONSTRAINT post_pkey PRIMARY KEY (id),
    CONSTRAINT post_type_fkey FOREIGN KEY (type)
        REFERENCES public.type (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.post
    OWNER to postgres;

CREATE TABLE public.value
(
    id integer NOT NULL DEFAULT nextval('value_id_seq'::regclass),
    field integer NOT NULL,
    post integer NOT NULL,
    value character varying COLLATE pg_catalog.default NOT NULL,
    CONSTRAINT value_pkey PRIMARY KEY (id),
    CONSTRAINT value_field_fkey FOREIGN KEY (field)
        REFERENCES public.field (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT value_post_fkey FOREIGN KEY (post)
        REFERENCES public.post (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.value
    OWNER to postgres;

CREATE TABLE public.link
(
    id integer NOT NULL DEFAULT nextval('link_id_seq'::regclass),
    field integer NOT NULL,
    post integer NOT NULL,
    target integer NOT NULL,
    CONSTRAINT link_pkey PRIMARY KEY (id),
    CONSTRAINT link_post_field_target_key UNIQUE (post, field, target),
    CONSTRAINT link_post_fkey FOREIGN KEY (post)
        REFERENCES public.post (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT link_target_fkey FOREIGN KEY (target)
        REFERENCES public.post (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT link_field_fkey FOREIGN KEY (field)
        REFERENCES public.field (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.link
    OWNER to postgres;

ALTER TABLE public.connection_id_seq OWNER TO postgres;
ALTER SEQUENCE public.connection_id_seq OWNED BY public.connection.id;

ALTER TABLE public.field_id_seq OWNER TO postgres;
ALTER SEQUENCE public.field_id_seq OWNED BY public.field.id;

ALTER TABLE public.fieldtype_id_seq OWNER TO postgres;
ALTER SEQUENCE public.fieldtype_id_seq OWNED BY public.fieldtype.id;

ALTER TABLE public.link_id_seq OWNER TO postgres;
ALTER SEQUENCE public.link_id_seq OWNED BY public.link.id;

ALTER TABLE public.type_id_seq OWNER TO postgres;
ALTER SEQUENCE public.type_id_seq OWNED BY public.type.id;

ALTER TABLE public.value_id_seq OWNER TO postgres;
ALTER SEQUENCE public.value_id_seq OWNED BY public.value.id;

ALTER TABLE public.post_id_seq OWNER TO postgres;
ALTER SEQUENCE public.post_id_seq OWNED BY public.post.id;

END;