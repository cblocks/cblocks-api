const Sentry = require('@sentry/node')
var log4js = require('log4js')
var fs = require('fs')
var sha256 = require('sha256')
var logger = log4js.getLogger()
const { prodSpecs, mainSpecs, testSpecs, postgressConnect } = require('./pg')
const { writeData } = require('./dbUtils')

const checkIfDbExists = async (db, specs) => {
  logger.debug(`checking if db ${specs.database} exists`)
  const result = await db.query(
    `SELECT datname FROM pg_catalog.pg_database
		WHERE lower (datname) = lower('${specs.database}')`
  )
  const exists =
    result.rowCount === 1 && result.rows[0].datname === specs.database
  logger.info(
    exists
      ? `db ${specs.database} exists`
      : `db ${specs.database} does not exist`
  )
  return exists
}

const createDb = async (db, specs) => {
  logger.info(`creating db ${specs.database}`)
  await db.query(
    `CREATE DATABASE ${specs.database}
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;`
  )
}

const dropDb = async (db, specs) => {
  logger.debug(`dropping db ${specs.database}`)
  await db.query(`DROP DATABASE IF EXISTS ${specs.database};`)
  logger.info(`dropped db ${specs.database}`)
}

const schemaUpToDate = async (db, specs, hash) => {
  logger.debug(`testing if ${specs.database} db is up to date`)
  try {
    const result = await db.query(
      `SELECT value FROM settings
			WHERE key = 'dbhash'`
    )
    logger.debug(`hash of current init script: ${hash}`)
    logger.debug(`hash of previous init script: ${result.rows[0].value}`)
    const upToDate = result.rows[0].value === hash
    logger.info(
      upToDate
        ? `${specs.database} db is up to date`
        : `${specs.database} db is not up to date`
    )
    return upToDate
  } catch (e) {
    logger.info(`${specs.database} db is not up to date: no settings-table`)
    return false
  }
}

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

const setupSchema = async (db, specs, queries) => {
  logger.info(`starting schema setup for db ${specs.database}`)
  const client = db.client()
  try {
    client.connect()
    await asyncForEach(queries, query => client.query(query))
    logger.info(`schema setup over for ${specs.database}`)
  } catch (e) {
    logger.error(e)
  } finally {
    client.end()
  }
}

const setupDb = async (mainDb, specs) => {
  await dropDb(mainDb, specs)
  await createDb(mainDb, specs)
}

const setupSchemaForDb = async (specs, queries, hash) => {
  const db = postgressConnect(specs)
  try {
    await setupSchema(db, specs, queries)
    await db.query(
      `INSERT INTO settings (key,value) VALUES ('dbhash','${hash}')`
    )
  } catch (e) {
    logger.error(e)
  } finally {
    db.pool.end()
  }
}

const writeDataForDb = async specs => {
  const db = postgressConnect(specs)
  try {
    await writeData(db)
  } catch (e) {
    logger.error(e)
  } finally {
    db.pool.end()
  }
}

const setupDbIfNecessary = async (mainDb, specs, queries, hash) => {
  const dbCheck = postgressConnect(specs)
  const upToDate = await schemaUpToDate(dbCheck, specs, hash)
  dbCheck.pool.end()
  if (!upToDate) {
    await setupDb(mainDb, specs, queries, hash)
    await setupSchemaForDb(specs, queries, hash)
    await writeDataForDb(specs)
  }
}

const readInitScript = () => {
  const file = fs.readFileSync('src/db/data/init_schema.sql').toString()
  const hash = sha256(file)
  const queries = file
    .replace(/(\r\n|\n|\r)/gm, ' ')
    .replace(/\s+/g, ' ')
    .split(';')
    .map(Function.prototype.call, String.prototype.trim)
    .filter(el => el.length != 0)
  return { queries, hash }
}

const dbinit = async () => {
  const mainDb = postgressConnect(mainSpecs)
  try {
    logger.info('starting db initialization')
    const mainExists = await checkIfDbExists(mainDb, mainSpecs)
    if (mainExists) {
      const { queries, hash } = readInitScript()
      await setupDbIfNecessary(mainDb, prodSpecs, queries, hash)
    }
    logger.info('finished db initialization')
  } catch (e) {
    Sentry.captureException(e)
    logger.error(e)
  } finally {
    mainDb.pool.end()
  }
}

const dbInitForTests = async () => {
  const mainDb = postgressConnect(mainSpecs)
  try {
    logger.info('starting db initialization for tests')
    const mainExists = await checkIfDbExists(mainDb, mainSpecs)
    if (mainExists) {
      const { queries, hash } = readInitScript()
      await setupDb(mainDb, testSpecs, queries, hash)
      await setupSchemaForDb(testSpecs, queries, hash)
    }
    logger.info('finished db initialization for tests')
  } catch (e) {
    Sentry.captureException(e)
    logger.error(e)
  } finally {
    mainDb.pool.end()
  }
}

module.exports.dbinit = dbinit
module.exports.dbInitForTests = dbInitForTests
module.exports.dropDb = dropDb
