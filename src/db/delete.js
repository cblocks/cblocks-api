const Sentry = require('@sentry/node')
var log4js = require('log4js')
var logger = log4js.getLogger()

const buildDeleteFunction = (
  queryString,
  keyString,
  refreshMessage,
  errorMessage
) => ({ db, refreshSchema }, { input: { [keyString]: id } }) =>
  db
    .query(queryString, [id])
    .then(res => {
      refreshSchema(refreshMessage + res.rows[0].name)
      return res.rowCount == 1 ? res.rows[0] : new Error(errorMessage)
    })
    .catch(e => {
      Sentry.captureException(e)
      logger.error(e)
      throw new Error(errorMessage)
    })

const route = buildDeleteFunction(
  'DELETE FROM public.route WHERE name = $1 RETURNING name, query',
  'name',
  'reloaded api after deleting route ',
  'unable to delete route'
)

const type = buildDeleteFunction(
  'DELETE FROM public.type WHERE id = $1 RETURNING id, name, label, description',
  'id',
  'reloaded api after deleting type ',
  'unable to delete type'
)

const fieldtype = buildDeleteFunction(
  'DELETE FROM public.fieldtype WHERE id = $1 RETURNING id, name, label, description',
  'id',
  'reloaded api after deleting fieldtype ',
  'unable to delete fieldtype'
)

const field = buildDeleteFunction(
  'DELETE FROM public.field WHERE id = $1 RETURNING id, type, index, shortform, fieldtype, name, label, description',
  'id',
  'reloaded api after deleting field ',
  'unable to delete field'
)

const generatedPost = ({ db }, { input: { id } }) =>
  db
    .query('DELETE FROM public.post WHERE id = $1 RETURNING id', [id])
    .then(res =>
      res.rowCount == 1 ? res.rows[0] : new Error('unable to delete post')
    )
    .catch(e => {
      Sentry.captureException(e)
      logger.error(e)
      throw new Error(`unable to delete post: ${e.detail}`)
    })

module.exports.type = type
module.exports.fieldtype = fieldtype
module.exports.field = field
module.exports.generatedPost = generatedPost
module.exports.route = route
