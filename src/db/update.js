const Sentry = require('@sentry/node')
var log4js = require('log4js')
var logger = log4js.getLogger()
const { checkReservedName, defaultFieldTypes } = require('../graphql/constants')

const buildUpdateFunction = (
  queryString,
  argumentExtractor,
  refreshMessage,
  errorMessage
) => ({ db, refreshSchema }, { input }) => {
  return db
    .query(queryString, argumentExtractor(input))
    .then(res => {
      refreshSchema(refreshMessage + input.name)
      return res.rowCount == 1 ? res.rows[0] : new Error(errorMessage)
    })
    .catch(e => {
      Sentry.captureException(e)
      logger.error(e)
      throw new Error(errorMessage)
    })
}

const route = buildUpdateFunction(
  'UPDATE public.route SET (query) = ($1) WHERE id = $2 RETURNING name, query',
  ({ name, query }) => [query, name],
  'reloaded api after updating route ',
  'unable to update route'
)

const type = buildUpdateFunction(
  'UPDATE public.type SET (name, label, description) = ($1, $2, $3) WHERE id = $4 RETURNING id, name, label, description',
  ({ id, name, label, description }) => [name, label, description, id],
  'reloaded api after updating type ',
  'unable to update type'
)

const fieldtype = async (
  { db, refreshSchema },
  { input: { id, name, label, description } }
) => {
  checkReservedName(name)
  const current = await db.query(
    'SELECT fieldtype FROM public.fieldtype WHERE id = $1',
    [id]
  )
  if (current.rowCount != 1) {
    return new Error('unable to update fieldtype')
  }
  const fieldtype = current.rows[0].fieldtype
  if (fieldtype == 1 || fieldtype == 2) {
    return new Error('cannot update protected fieldtype')
  }
  return db
    .query(
      'UPDATE public.fieldtype SET (name, label, description) = ($1, $2, $3) WHERE id = $4 RETURNING id, name, label, description',
      [name, label, description, id]
    )
    .then(res => {
      refreshSchema(`reloaded api after updating fieldtype ${name}`)
      return res.rowCount == 1
        ? res.rows[0]
        : new Error('unable to update fieldtype')
    })
    .catch(e => {
      Sentry.captureException(e)
      logger.error(e)
      throw new Error(`unable to update fieldtype: ${e.detail}`)
    })
}

const field = async (
  { db, refreshSchema },
  { input: { id, index, fieldtype, name, label, description, shortform } }
) => {
  checkReservedName(name)
  const current = await db.query(
    'SELECT fieldtype FROM public.field WHERE id = $1',
    [id]
  )
  if (current.rowCount != 1) {
    return new Error('unable to update field')
  }
  const currentFieldtype = current.rows[0].fieldtype
  if (currentFieldtype == 1 || currentFieldtype == 2) {
    if (currentFieldtype != fieldtype) {
      return new Error(
        'cannot update fieldtype of fields with protected fieldtype'
      )
    }
  } else if (fieldtype == 1 || fieldtype == 2) {
    return new Error('cannot change fieldtype to protected fieldtype')
  }
  return db
    .query(
      'UPDATE public.field SET (index, fieldtype, name, label, description, shortform) = ($1, $2, $3, $4, $5, $6) WHERE id = $7 RETURNING id, type, index, fieldtype, name, label, description, shortform',
      [index, fieldtype, name, label, description, shortform, id]
    )
    .then(res => {
      refreshSchema(`reloaded api after updating field ${name}`)
      return res.rowCount == 1
        ? res.rows[0]
        : new Error('unable to update field')
    })
    .catch(e => {
      Sentry.captureException(e)
      logger.error(e)
      throw new Error(`unable to update field: ${e.detail}`)
    })
}

const fieldString = (f, args) => {
  switch (f.fieldtype) {
    case defaultFieldTypes.connection:
      return linkString(f, args)
    case defaultFieldTypes.connections:
      return linksString(f, args)
    default:
      return valueString(f, args)
  }
}

const valueString = ({ name, id }, { id: postId, [name]: value }) => {
  if (value === null) {
    return [
      [
        `DELETE FROM public.value WHERE post = ${postId} AND field = ${id} RETURNING null AS "${name}"`
      ]
    ]
  } else {
    return [
      [`DELETE FROM public.value WHERE post = ${postId} AND field = ${id}`],
      [
        `INSERT INTO public.value (post, field, value) VALUES (${postId},${id},$1) RETURNING value AS "${name}"`,
        [value]
      ]
    ]
  }
}

const linkString = ({ name, id }, { id: postId, [name]: target }) => {
  if (target === null) {
    return [
      [`DELETE FROM public.link WHERE post = ${postId} AND field = ${id}`]
    ]
  } else {
    return [
      [`DELETE FROM public.link WHERE post = ${postId} AND field = ${id}`],
      [
        `INSERT INTO public.link (post, field, target) VALUES (${postId},${id},$1) RETURNING target AS "${name}"`,
        [target]
      ]
    ]
  }
}

const linksString = ({ name, id }, args) => [
  [`DELETE FROM public.link WHERE post = ${args.id} AND field = ${id}`],
  ...args[name].map(target => [
    `INSERT INTO public.link (post, field, target) VALUES (${
      args.id
    },${id},$1) RETURNING target AS "${name}"`,
    [target]
  ])
]

const buildSqlForGeneratedPost = (typeDefinition, { input: args }) =>
  typeDefinition.fields.reduce(
    (fieldsql, f) =>
      args[f.name] === undefined
        ? fieldsql
        : [...fieldsql, ...fieldString(f, args)],
    []
  )

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

const dollarList = i => {
  const params = [...Array(i).keys()].map(i => `$${i + 1}`)
  return `(${params.join(',')})`
}

const checkFkIntegrity = async (db, f, { input }) => {
  if (
    input[f.name] == null ||
    input[f.name] == undefined ||
    input[f.name].length == 0
  ) {
    return
  }
  const res = await db.query(
    `SELECT type FROM public.post WHERE id ${
      f.fieldtype == 1 ? '= $1' : `IN ${dollarList(input[f.name].length)}`
    }`,
    f.fieldtype == 1 ? [input[f.name]] : [...input[f.name]]
  )
  if (
    f.fieldtype == 1
      ? res.rows.length != 1
      : res.rows.length != input[f.name].length
  ) {
    throw new Error(`not all referenced posts exist`)
  }
  if (res.rows.some(r => r.type != f.target)) {
    throw new Error(`not all referenced posts have the correct type`)
  }
}

const checkPostType = async (db, td, { input: { id } }) => {
  const res = await db.query(`SELECT type FROM public.post WHERE id = ${id}`)
  if (!res.rows[0]) {
    throw new Error(`post does not exist`)
  }
  if (res.rows[0].type != td.id) {
    throw new Error(`post is not of type specified by query`)
  }
}

const generatedPost = async ({ db, typeDefinition }, args) => {
  const typeFields = typeDefinition.fields.filter(
    f => f.fieldtype == 1 || f.fieldtype == 2
  )
  try {
    await checkPostType(db, typeDefinition, args)
  } catch (e) {
    Sentry.captureException(e)
    logger.error(e)
    throw e
  }
  try {
    await asyncForEach(typeFields, f => checkFkIntegrity(db, f, args))
  } catch (e) {
    Sentry.captureException(e)
    logger.error(e)
    throw e
  }
  const client = await db.pool.connect()
  try {
    client.connect()
    await client.query('BEGIN')
    const result = await buildSqlForGeneratedPost(typeDefinition, args).reduce(
      async (acc, sql) => {
        const obj = await acc
        const res = await client.query(...sql)
        if (!res.rows[0]) {
          return obj
        }
        const key = Object.keys(res.rows[0])[0]
        const fieldDefinition = typeDefinition.fields.find(f => f.name == key)
        if (fieldDefinition.fieldtype == 2) {
          if (obj[key]) {
            return { ...obj, [key]: [...obj[key], res.rows[0][key]] }
          } else {
            return { ...obj, [key]: [res.rows[0][key]] }
          }
        } else {
          return { ...obj, ...res.rows[0] }
        }
      },
      Promise.resolve({ id: args.input.id })
    )
    await client.query('COMMIT')
    return result
  } catch (e) {
    Sentry.captureException(e)
    logger.error(e)
    await db.query('ROLLBACK')
    throw new Error(`unable to update post: ${e.detail}`)
  } finally {
    client.release()
  }
}

module.exports.type = type
module.exports.fieldtype = fieldtype
module.exports.field = field
module.exports.generatedPost = generatedPost
module.exports.route = route
