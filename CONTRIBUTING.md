# Contributing

## Prerequisites
* [Node](https://nodejs.org/en/)
* [NPM](https://www.npmjs.com)
* [Docker](https://www.docker.com/get-started)

## Getting started
To start developing:
```bash
npm run dev
```
This will start webpack, jest, a basic webserver on [localhost:3000](http://localhost:3000), documentation on [localhost:4001](http://localhost:4001) and a postgres server on [localhost:5432](http://localhost:5432).

## How to contribute
Clone the project and work only on feature branches. Never work on master. If your feature is ready to be merge create a pull request.

## Coding Rules
To ensure consistency throughout the source code, keep these rules in mind as you are working:

* All features or bug fixes must be tested using [jest](https://facebook.github.io/jest/).
* Everything should be documented using [jsdoc](http://usejsdoc.org).
* Follow [standardjs](https://standardjs.com/) style.

## Commit Message Guidelines
Follow [angular](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#commit) commit message guidelines.

## License
By contributing to this project, you agree that your contributions will be licensed under its MIT license.
