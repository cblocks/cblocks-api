# cblocks-api
[![pipeline status](https://gitlab.com/cblocks/cblocks-api/badges/master/pipeline.svg)](https://gitlab.com/cblocks/cblocks-api/commits/master)
[![coverage report](https://gitlab.com/cblocks/cblocks-api/badges/master/coverage.svg)](https://gitlab.com/cblocks/cblocks-api/commits/master)
[![Inline docs](http://inch-ci.org/github/cblocks/cblocks-api.svg?branch=master&style=shields)](http://inch-ci.org/github/cblocks/cblocks-api)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/6389ee5059104a399c9c73c075847e95)](https://www.codacy.com/app/simonbreiter/cblocks-api?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=cblocks/cblocks-api&amp;utm_campaign=Badge_Grade)
[![Known Vulnerabilities](https://snyk.io/test/github/cblocks/cblocks-api/badge.svg)](https://snyk.io/test/github/cblocks/cblocks-api)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

A headless cms solution.

## How to contribute
To contribute to this project, please read our [guidelines](./CONTRIBUTING.md) first.
